solution(S) :-
	eightUniqueColumns(S),
	safeRows(S).

eightUniqueColumns([1/_,2/_,3/_,4/_,5/_,6/_,7/_,8/_]).

safeRows([]).
safeRows([ X / Y | Rest ]) :- 
            safeRows(Rest),
			      member(Y, [1,2,3,4,5,6,7,8]), 
			      noAttack( X / Y, Rest).

noAttack(_,[]).
noAttack(X / Y, [X1 / Y1 | Rest]) :- 
             Y =\= Y1,
				     abs(X-X1) =\= abs(Y-Y1),
				     noAttack(X / Y, Rest).
