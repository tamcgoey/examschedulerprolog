% COMP3649 - Exam Scheduler Assignment 3
% Thomas McGoey-Smith (tmcgo743)
% Sched Test Driver
:- include('CourseData.pl').
:- use_module('Scheduler.pl').
:- use_module('Util.pl').
:- use_module('Processing.pl').

sched(CourseDataFile,NumSlots) :-
  fileToData(CourseDataFile,CourseData),
  schedule(CourseData,NumSlots,Schedule), % only print out 1 schedule
  prettyPrinter(Schedule).

/* Format the created schedule */
prettyPrinter([]).
prettyPrinter([(Name,Slot)|ScheduleRest]) :-
  write(Name),write('\t'),write(Slot),nl,
  prettyPrinter(ScheduleRest).


/* Parse a file using codes */
fileToData(CourseDataFile,CourseData) :-
  read_file_to_codes(CourseDataFile,Codes,[]),
  words(Codes,ConvertedCodes),
  processData(ConvertedCodes,CourseData).
