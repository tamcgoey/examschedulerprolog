:- module( processing,
            [ 
              processData/2
            ]
          ).

:- use_module('Util.pl').


processData(Codes,CourseData) :-
  pData(Codes,[],CourseData).

% Helper to process list (Haskell-esk)
pData([],FinalData,FinalData).

pData([H|T],[],FinalData) :-
  pData(T,[(H,[])],FinalData).

pData([H|T],[(CD_Head,CD_ids)|CD_Tail],FinalData) :-
  if(
      integer(H),
      pData(T,[(CD_Head,[H|CD_ids])|CD_Tail],FinalData),
      (
        append([(H,[])],[(CD_Head,CD_ids)|CD_Tail],NewList),
        pData(T,NewList,FinalData)
      )
    ).