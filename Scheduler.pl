:- module( scheduler,
        [
          schedule/3
        ]
      ).

:- use_module('Course.pl').
:- use_module('Util.pl').

schedule(CourseData,NumSlots,Schedule) :-
  maplist(newCourse(NumSlots),CourseData,Courses),  % Create all courses
  maplist(setAdjacent(Courses),Courses,CourseList), % Create adjoined list of courses
  solvr(CourseList,[],Schedule).

solvr([],FinalSchedule,FinalSchedule).
solvr([course(Name,_,Adj,Timeslots) | CourseList],Schedule,FinalSchedule) :-
    member(T,Timeslots),
    NewSchedule = [(Name,T) | Schedule],
    updateCourseList(T,Adj,CourseList,[],UpdatedCourseList),
    solvr(UpdatedCourseList,NewSchedule,FinalSchedule).

    
updateCourseList(_,_,[],FinalList,FinalList).
updateCourseList(T,AdjCourses,[course(Name,Ids,Adj,TimeSlots) | Rest],List,FinalList) :-
  if(
      (member(Name,AdjCourses)),
      (ruleOutSlot(T,course(Name,Ids,Adj,TimeSlots),NewCourse)),
      (NewCourse = course(Name,Ids,Adj,TimeSlots))
    ),
  NewList = [NewCourse | List],
  updateCourseList(T,AdjCourses,Rest,NewList,FinalList).