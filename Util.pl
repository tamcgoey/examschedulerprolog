% util.pl

:- module( util,
          [
            if/2,
            if/3,
            foldr/4,
            words/2,
            word/2
          ]
          ).

:- module_transparent if/2.
:- module_transparent if/3.
:- module_transparent foldr/4.
:- module_transparent words/2.
            
% we have to build if using !
if(Test,Then) :- if(Test,Then,true).
if(Test,Then,Else) :- Test, !, Then ; Else.

foldr(_,G,[],G) :- !.
foldr(F,G,[X | XS], A) :- foldr(F,G,XS,A2),
                          call(F,X,A2,A).

isSpace(Ch) :- code_type(Ch,space).
isNotSpace(Ch) :- not(isSpace(Ch)).

split(_,[],[],[]).
split(Pred, [X|XS],Pass,Rest):-
    if( call(Pred,X),
      ( split(Pred,XS,Pass2,Rest),
      Pass=[X|Pass2]
    ),
    ( Pass = [],
      Rest = [X|XS]
    )
    ).  

word(WordCodes,Word) :-
      WordCodes = [ FirstCode | _ ],
    if( code_type(FirstCode,digit),
        number_codes(Word,WordCodes),
      atom_codes(Word,WordCodes)
    ).
    
words([],[]) :- !.
words(Codes,Words) :-
     split(isSpace, Codes, _, Rest),
   split(isNotSpace, Rest, WordCodes, Rest2),
   if( WordCodes = [],
       Words = [],
     ( word(WordCodes,Word),
       words(Rest2,Words2),
       Words=[Word|Words2]
     )
     ).