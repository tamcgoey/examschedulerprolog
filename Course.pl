% COMP3649 - Exam Scheduler Assignment 3
% Thomas McGoey-Smith (tmcgo743)
% Course Module
:- module( course,
            [ 
              newCourse/3,
              setAdjacent/3,
              ruleOutSlot/3
            ]
          ).

% generates a new course
newCourse(NumSlots,(Name,Ids),course(Name,Ids,[],Slots)) :-
  numlist(1,NumSlots,Slots).

setAdjacent(ListOfCourses,course(Name,Ids,_,Slots),
            course(Name,Ids,Adjacents,Slots) ) :-
              foldr(compareCourse(Name,Ids),
                [],
                ListOfCourses,
                Adjacents).   


/*
ruleOutSlot(Slot,course(Name,Ids,Adj,Slots1),course(Name,Ids,Adj,Slots2)) :- 
  if(select(Slot,Slots1,[_|_]),
      select(Slot,Slots1,Slots2),
      fail
    ). % as per the recommendation on having 


*/

ruleOutSlot(Slot,
            course(Name, Ids, Adj, Slots1),
            course(Name, Ids, Adj, Slots2)) :- 
              subtract(Slots1,[Slot],Slots2),
              Slots2 = [_|_].                   % not empty
% Helpers

% helper for the above 
compareCourse(Name1,Ids1,course(Name2,Ids2,_,_),Adjacent,AdjacentAdded) :- 
  if( (Name1 = Name2 ; intersection(Ids1,Ids2,[])) , % if its the same course, or if they do
      Adjacent = AdjacentAdded,                      % not have any overlapping students, then set the adjacent to the adjacent added
      AdjacentAdded = [Name2 | Adjacent]             % else, change the AdjacentAdded to append the course name to the adjacent list
    ).
    
% we have to build if using !
if(Test,Then) :- if(Test,Then,true).
if(Test,Then,Else) :- Test, !, Then ; Else.
  
foldr(_,G,[],G) :- !.
foldr(F,G,[X | XS], A) :- foldr(F,G,XS,A2),
                          call(F,X,A2,A).